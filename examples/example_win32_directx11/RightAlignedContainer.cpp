#include "RightAlignedContainer.h"



RightAlignedContainer::RightAlignedContainer()
{
}


RightAlignedContainer::~RightAlignedContainer()
{

}

RightAlignedContainer::RightAlignedContainer(std::initializer_list<IWidget*> widgets) : BaseContainer(widgets) {}


bool RightAlignedContainer::Draw(ImVec4 displayArea)
{
	// figure out the size of everything
	ImVec2 size = CalculateSize();

	ImVec2 startPos(
		displayArea.z - (size.x + (ImGui::GetStyle().WindowPadding.x) + (2 * ImGui::GetStyle().FramePadding.x)),
		0
	);
	// for a right aligned container we simply
	// start the cursor at the point that's our 
	// total size away from the right hand side
	// set the cursor to the starting point
    ImVec2 currentPos = ImGui::GetCursorPos();

	ImVec2 finalPos(
		currentPos.x + startPos.x,
		currentPos.y
	);

	ImVec4 drawPosition
	{
		displayArea.x,
		displayArea.y,
		finalPos.x,
		finalPos.y,
	};
	for (size_t i = 0; i < m_widgets.size(); ++i)
	{

		// draw each item in it's correct right aligned position
		DrawItem(*m_widgets[i], drawPosition);

		drawPosition.z += m_widgets[i]->CalculateSize().x + ImGui::GetStyle().ItemSpacing.x;
		// keep everything on the same line 
		// by calling SameLine() for every widget except the last
		if (i != m_widgets.size() - 1)
		{
			ImGui::SameLine();
		}
	}

	return false;
}
