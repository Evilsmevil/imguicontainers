#pragma once
#include "BaseContainer.h"

class LeftAlignedContainer : public BaseContainer
{
public:
    LeftAlignedContainer();
    virtual ~LeftAlignedContainer();

    virtual bool Draw(ImVec4 displayArea) override;
};

