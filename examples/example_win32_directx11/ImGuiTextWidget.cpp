#include "ImGuiTextWidget.h"

ImGuiTextWidget::ImGuiTextWidget(const char* text)
{
	m_text = text;
}


ImGuiTextWidget::~ImGuiTextWidget()
{
}

ImVec2 ImGuiTextWidget::CalculateSize()
{
	auto textSize = ImGui::CalcTextSize(m_text);
	return textSize;
}

bool ImGuiTextWidget::Draw(ImVec4 drawArea)
{
	ImVec2 startPos = { drawArea.z, drawArea.w };
	ImGui::SetCursorPos(startPos);

	ImGui::Text(m_text);
	return false;
}
