#pragma once
#include "IWidget.h"

class ImGuiTextWidget : public IWidget
{
public:
	ImGuiTextWidget(const char* text);
	~ImGuiTextWidget();

	// Inherited via IWidget
	virtual ImVec2 CalculateSize() override;
	virtual bool Draw(ImVec4 displayArea) override;
	virtual bool IsLayoutContainer() override { return false; }

private:
	const char* m_text;
};

