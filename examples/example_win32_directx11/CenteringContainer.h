#pragma once
#include "ImGuiButtonWidget.h"
#include "IWidget.h"
#include "BaseContainer.h"
#include <functional>
#include <vector>

class CenteringContainer : public BaseContainer
{
public:
	CenteringContainer();
	CenteringContainer(std::initializer_list<IWidget*> widgets);
	virtual ~CenteringContainer() {};
	virtual bool Draw(ImVec4 displayArea) override;
	
};

