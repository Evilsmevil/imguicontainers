#include "BaseContainer.h"
#include "imgui.h"
#include "ImGuiButtonWidget.h"
#include "ImGuiTextWidget.h"

BaseContainer::BaseContainer()
{
}

BaseContainer::BaseContainer(std::initializer_list<IWidget*> widgets)
{
	for (IWidget* widget : widgets)
	{
		AddWidget(widget);
	}
}

BaseContainer::~BaseContainer()
{
	// free all the memory here - obviously we would use better memory mangaement than this for realsies
	//for (IWidget* widget : m_widgets)
	//{
	//	delete (widget);
	//}
}


void BaseContainer::AddWidget(IWidget* widget)
{
	m_widgets.push_back(widget);
}

ImVec2 BaseContainer::CalculateSize()
{
	ImVec2 size;
	auto style = ImGui::GetStyle();
	for (IWidget* widget : m_widgets)
	{
		ImVec2 itemSize = widget->CalculateSize();
		size.x += itemSize.x;
	}

	// we only need item spacing for count - 1 items as the last item won't need spacing
	float itemSpacingX = (m_widgets.size() - 1) * (style.ItemSpacing.x * 2);
	float itemSpacingY = style.ItemSpacing.y;

	size.x += itemSpacingX;
	size.y += itemSpacingY;

	return size;
}

void BaseContainer::DrawItem(IWidget & widget, ImVec4 displayableArea)
{
	widget.Draw(displayableArea);
}
