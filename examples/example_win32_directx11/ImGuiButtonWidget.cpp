#include "ImGuiButtonWidget.h"
#include "imgui.h"


ImGuiButtonWidget::ImGuiButtonWidget(const char* label, std::function<void()> onClick)
{
	m_label = label;
	m_onClick = onClick;
}


ImGuiButtonWidget::~ImGuiButtonWidget()
{
}

ImVec2 ImGuiButtonWidget::CalculateSize()
{
	auto textSize = ImGui::CalcTextSize(m_label);
	ImVec2 size(
		textSize.x,
		textSize.y);

	size.x += (ImGui::GetStyle().FramePadding.x * 2);

	return size;
}

bool ImGuiButtonWidget::Draw(ImVec4 drawArea)
{
	ImVec2 startPos = { drawArea.z, drawArea.w };

	ImGui::SetCursorPos(startPos);
	if (ImGui::Button(m_label))
	{
		if (m_onClick != nullptr)
		{
			m_onClick();
		}
	}
	return false;
}
