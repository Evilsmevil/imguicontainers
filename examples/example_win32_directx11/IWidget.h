#pragma once
#include "imgui.h"

class IWidget
{
public:
	virtual ImVec2 CalculateSize() =0 ;
	virtual bool Draw(ImVec4 extents) = 0;
	virtual bool IsLayoutContainer() = 0;
};