#pragma once

class AnimationManager;
namespace ImGui
{
    bool TimedAlert(const char* text, float duration, AnimationManager& animManager);
}