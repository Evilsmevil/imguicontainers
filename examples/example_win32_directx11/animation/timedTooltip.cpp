#include "timedTooltip.h"
#include "animMgr.h"
#include <imgui.h>

namespace ImGui
{
    void TimedTooltip(const char * label, float hoverTime, AnimationManager& animMgr)
    {
        if(ImGui::IsItemHovered())
        {
            ImGui::PushID("tooltipTimer");
            float timeAliveTooltip = animMgr.TimeAlive(ImGui::GetID(label));
            if (timeAliveTooltip > hoverTime)
            {
                ImGui::SetTooltip(label);
            }
            ImGui::PopID();
        }
    }
}