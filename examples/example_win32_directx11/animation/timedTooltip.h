#pragma once

class AnimationManager;
namespace ImGui
{
    void TimedTooltip(const char* label, float hoverTime, AnimationManager& animManager);
}