#include "timedAlert.h"

#include "animMgr.h"
#include "imgui.h"
#include <algorithm>

bool ImGui::TimedAlert(const char * text, float duration, AnimationManager & animManager)
{
    // show the alert only for a few seconds and then stop
    ImGui::PushID("alerter");

    auto currentCol = ImColor(1.0f,0.0f,0.0f);
    float timeAlive = animManager.TimeAlive(ImGui::GetID("alerter"));

    float alpha = std::min(timeAlive / 1.f, 1.0f);
    currentCol.Value.w = alpha;
    ImGui::PushStyleColor(ImGuiCol_Text, currentCol.Value);
    bool show = timeAlive < duration;

    if(show)
    {
        ImGui::Text(text);
    }

    ImGui::PopStyleColor(); 
    ImGui::PopID();
    return show;
}
