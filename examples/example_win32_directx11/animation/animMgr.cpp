#include "animMgr.h"
#include <time.h>
#include <functional>
#include <windows.h>

float AnimationManager::ReadAndUpdateExistingState(AnimState& animState, unsigned long curTickMs)
{
    animState.m_readThisFrame = true;

    return (curTickMs - animState.m_startTime) / 1000.f;
}

float AnimationManager::CreateNewAnimStateRecord(const unsigned int id, unsigned long curTickMs)
{
    m_animStates.emplace(id, AnimState(curTickMs));

    // if the anim state was created this frame then it's not run yet.
    return 0;
}

float AnimationManager::TimeAlive(const unsigned int id)
{
    // if we find the state then return the time alive and update it's 'read' flag
    auto animState = m_animStates.find(id);
    if (animState != m_animStates.end())
    {
        return ReadAndUpdateExistingState(animState->second, m_currentTime);
    }
    else
    {
        return CreateNewAnimStateRecord(id, m_currentTime);
    }
}

void AnimationManager::OnFrameStart()
{
    m_currentTime = GetTickCount();
}

void AnimationManager::OnFrameEnd()
{
    // remove any entries that were not called this frame
    for (auto it = begin(m_animStates); it != end(m_animStates);)
    {
        if (it->second.m_readThisFrame == false)
        {
            it = m_animStates.erase(it);
        }
        else
        {
            // un-set our flag if we're not erasing
            it->second.m_readThisFrame = false;
            ++it;
        }
    }
}
