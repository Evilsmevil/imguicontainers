#pragma once
#include <unordered_map>

class AnimationManager
{
public:
    float TimeAlive(const unsigned int id);

    void OnFrameStart();
    void OnFrameEnd();
private:
    struct AnimState
    {
        AnimState(unsigned long startTime)
            : m_startTime(startTime),
              m_readThisFrame(true) // we're implicitly read when we're created
        {}

        unsigned long m_startTime;
        bool m_readThisFrame;
    };

    std::unordered_map<unsigned int, AnimState> m_animStates;

private:
    float ReadAndUpdateExistingState(AnimState& animState, unsigned long curTickMs);
    float CreateNewAnimStateRecord(const unsigned int, unsigned long curTickMs);

    unsigned long m_currentTime;
};