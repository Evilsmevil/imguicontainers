#pragma once
#include "ILayoutContainer.h"
#include "IWidget.h"

#include <vector>

class IWidget;

class BaseContainer : public ILayoutContainer, public IWidget
{
public:
	BaseContainer();
	BaseContainer(std::initializer_list<IWidget*> widgets);

	virtual ~BaseContainer();

	virtual void AddWidget(IWidget* widget);
	virtual ImVec2 CalculateSize() override;
	virtual bool IsLayoutContainer() override { return true; }

protected:
	std::vector<IWidget*> m_widgets;
	virtual void DrawItem(IWidget& widget, ImVec4 displayableArea);
};

