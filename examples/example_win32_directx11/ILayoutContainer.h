#pragma once
#include <functional>
#include "imgui.h"
#include "IWidget.h"

class ILayoutContainer
{
	virtual void AddWidget(IWidget* widget) = 0;
	virtual bool Draw(ImVec4 displayArea) = 0;
};