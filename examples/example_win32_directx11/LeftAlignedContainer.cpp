#include "LeftAlignedContainer.h"
#include "IWidget.h"


LeftAlignedContainer::LeftAlignedContainer()
{
}


LeftAlignedContainer::~LeftAlignedContainer()
{
}

bool LeftAlignedContainer::Draw(ImVec4 displayArea)
{
    // The simplest container type, measure the widget and add spacing
    float cursorX = displayArea.x;

    for(size_t i = 0; i < m_widgets.size(); ++i)
    {
	    ImVec2 currentPos = ImGui::GetCursorPos();

        IWidget* widget = m_widgets[i];
        
        DrawItem(*widget, {displayArea.x, currentPos.y, cursorX, currentPos.y});
        
        float itemWidth = widget->CalculateSize().x;
        cursorX += (itemWidth + ImGui::GetStyle().ItemSpacing.x);

        // keep everything on the same line 
		// by calling SameLine() for every widget except the last
		if(i != m_widgets.size() - 1)
		{
			ImGui::SameLine();
		}
    }

    return false;
}
