#pragma once
#include "ILayoutContainer.h"
#include "BaseContainer.h"

class RightAlignedContainer : public BaseContainer
{
public:
	RightAlignedContainer();
	RightAlignedContainer(std::initializer_list<IWidget*> widgets);
	virtual ~RightAlignedContainer();

	// Inherited via ILayoutContainer
	virtual bool Draw(ImVec4 displayArea) override;
};

