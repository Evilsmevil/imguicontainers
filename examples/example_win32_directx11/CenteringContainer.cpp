#include "CenteringContainer.h"
#include "ImGuiTextWidget.h"
CenteringContainer::CenteringContainer()
{
}

CenteringContainer::CenteringContainer(std::initializer_list<IWidget*> widgets) : BaseContainer(widgets) {}


bool CenteringContainer::Draw(ImVec4 displayArea)
{
	// figure out the size of everything
	ImVec2 size = CalculateSize();

	// center of the display area
	ImVec2 centerPoint(
		displayArea.z * 0.5f,
		0);

	// where we should put the cursor
	ImVec2 startingPoint(
		centerPoint.x - (size.x * 0.5f),
		0
	);

	ImVec2 currentPos = ImGui::GetCursorPos();

	ImVec2 finalPos(
		startingPoint.x + ImGui::GetStyle().WindowPadding.x,
		currentPos.y
	);

	float currentWidth = finalPos.x;
	float lastWidth = displayArea.x;
	for (size_t i = 0; i < m_widgets.size(); ++i)
	{
		// Accumulate size by measuring each widget
		ImVec4 nextWidgetPos = {
			lastWidth, //TODO we need to track the last extent
			displayArea.y,
			currentWidth,
			currentPos.y
		};

		DrawItem(*m_widgets[i], nextWidgetPos);
		
		lastWidth = currentWidth;
		float widgetWidth = m_widgets[i]->CalculateSize().x;
		currentWidth += widgetWidth;
		currentWidth += ImGui::GetStyle().ItemSpacing.x;

		// keep everything on the same line 
		// by calling SameLine() for every widget except the last
		if(i != m_widgets.size() - 1)
		{
			ImGui::SameLine();
		}
	}

	return false;
}








