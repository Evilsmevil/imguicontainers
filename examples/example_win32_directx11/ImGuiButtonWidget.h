#pragma once
#include "IWidget.h"
#include <functional>

class ImGuiButtonWidget : public IWidget
{
public:
	ImGuiButtonWidget(const char* label, std::function<void()> onClick);
	virtual ~ImGuiButtonWidget();

	// Inherited via ISizedWidget
	virtual ImVec2 CalculateSize() override;
	virtual bool Draw(ImVec4 displayableArea) override;
	virtual bool IsLayoutContainer() override { return false; }
private:
	const char* m_label;
	std::function<void()> m_onClick;
};

